\c workflow;

CREATE TABLE ops.onyx_dq_rule_result (
  id INT NOT NULL IDENTITY (1,1) encode raw,
  batch_id INT encode LZO,
  uid varchar(80) encode LZO,
  run_date date,
  ruleid INT encode LZO,
  query_1 varchar(200) encode LZO,
  query_2 varchar(200) encode LZO,
  query_1_result varchar(20) encode LZO,
  query_2_result varchar(20) encode LZO,
  difference decimal(12,2) encode LZO,
  operator varchar(4) encode LZO,
  comments varchar(100),
  result char(1)
)
DISTSTYLE KEY DISTKEY (uid) INTERLEAVED SORTKEY (uid, run_date) ;
