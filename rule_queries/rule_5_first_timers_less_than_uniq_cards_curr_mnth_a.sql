select (total_unique_cards/active_stores) as avg_unique from PARAM_RS_ONYX_SCHEMA:.monthly_brand_overview 
a join report_info b
on a.yyyymm = b.report_month and a.start_date = b.param_current_month_start_date
where a.start_date = cast (cast(PARAM_CURRENT_MONTH_START_DATE: as varchar(10)) as date)
and uid='%s';
