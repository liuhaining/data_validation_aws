	
import os
import psycopg2
import pymysql
import ConfigParser
import sharedlib.utils as utils
import traceback
import sys
from subprocess import call,Popen,PIPE
import subprocess


def query_mysql(rs_onyx_cur,query):

	rs_onyx_cur.execute(query)
	header = [i[0] for i in rs_onyx_cur.description]
	rows = [list(i) for i in rs_onyx_cur.fetchall()]
	rows.insert(0,header)
	rs_onyx_cur.close()
	return rows
 
#take list of lists as argument	
def nlist_to_html(list2d,query):
	#bold header  #'+unicode(str(query)+ u'
	htable=u'<html><body> Query: '+ query +'\n'+'<table border="1" bordercolor=000000 cellspacing="0" cellpadding="1" style="background-color: #93B874;table-layout:fixed;vertical-align:bottom;font-size:13px;font-family:verdana,sans,sans-serif;border-collapse:collapse;border:1px solid rgb(130,130,130);" >'
	list2d[0] = [u'<b>' + i + u'</b>' for i in list2d[0]] 
	for row in list2d:
		newrow = u'<tr>' 
		newrow += u'<td align="left" style="padding:1px 4px">'+unicode(row[0])+u'</td>'
		row.remove(row[0])
		newrow = newrow + ''.join([u'<td align="right" style="padding:1px 4px">' + unicode(x) + u'</td>' for x in row])  
		newrow += '</tr>' 
		htable+= newrow
	htable += '</table> </body></html>'
	return htable
 
def sql_html(rs_onyx_cur,batch_id,project,project_batch,config):

	try:
#		query = """  select * from ops.pdf_gen_result_matrix where batch_id = %s ; """%(batch_id)  # batch id has to come from executer
		query = """SELECT id,uid,project,project_batch,batch_id,is_data_quality from %s.%s where project = '%s' and project_batch = '%s' and batch_id = %s and is_view_created = %s order by id"""%(config.get('redshift','rs_ops_schema'),config.get('redshift', 'onyx_pdf_gen_matrix_tbl'),project,project_batch,batch_id,2)
		HTMLpath = './summary.html' # get temp path and substitue this here
		if os.path.isfile(HTMLpath):
			os.remove(HTMLpath)  # remove the file
		f = open(HTMLpath, 'w')
		f.write(nlist_to_html(query_mysql(rs_onyx_cur,query),query))
		f.close()
		send_email(query,HTMLpath,batch_id)
		return 
	except Exception as e:
		print(str(traceback.format_exc()))
		raise
	finally:
		print "Inside finally"

def send_email(query,file_to_attach,batch_id):
	subject = "\'TitanWF-AWS: Onyx - Status for batch : " + str(batch_id) +"\' "  # batch id has to come from executer

	# cmd="sh echo \" "+ query +" \" | mail -a \"" +file_to_attach + "\"  -s " +subject + "Virendhar.Sivaraman@firstdata.com"
	cmd="mutt -e 'set content_type=text/html' -s " +subject + "VijayaSarathy.Paranthaman@firstdata.com" + " < " + file_to_attach
	print cmd
	p=subprocess.Popen(cmd, stderr=subprocess.STDOUT, stdout=subprocess.PIPE,shell=True)
	stdout,stderr = p.communicate()
	if p.returncode != 0:
		print "email send failed"
	else:
		print "email sent"


if __name__ == '__main__':

	try:
		send_email_sh = "test"
		config = ConfigParser.ConfigParser(allow_no_value=True)
		basepath = os.path.abspath(os.path.dirname(__file__))
		head, tail = os.path.split(basepath)
		config.read(os.path.join(head, 'config', 'wf_aws_onyx_config.ini'))
		rs_host = config.get('redshift', 'rs_host')
		rs_user = config.get('redshift', 'rs_user')
		rs_pwd = config.get('redshift', 'rs_pwd')
		rs_db = config.get('redshift', 'rs_onyx_db')
		rs_port= int(config.get('redshift', 'rs_port'))
		rs_src_schema= config.get('redshift', 'rs_src_schema')
		rs_onyx_conn = psycopg2.connect(host = rs_host, port = rs_port, dbname = rs_db, user = rs_user, password = rs_pwd)
		rs_onyx_cur = rs_onyx_conn.cursor()

		# Collecting variables
		data = execute()

	except Exception as e:
		raise	
		logger.info(str(traceback.format_exc()))
	
