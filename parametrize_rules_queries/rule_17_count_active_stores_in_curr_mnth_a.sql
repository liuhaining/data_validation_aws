-- Rule 17:-
-- Count of active store in current month

-- query 1 :- q1 = select active_stores from nya_joe_restaurant.monthly_brand_overview where start_date = '2016-09-01' and end_date = '2016-09-30';

-- query 2 :- q2= select AVG(active_stores) from nya_joe_restaurant.weekly_brand_performance_trend where year='2016' and week_no IN ('35', '36', '37', '38', '39');

-- q1 = q2 (almost maybe a small difference because the week is sat and sun and may have few different days of month).

-- Need to parametrize week no for current month as well

select active_stores from %s.monthly_brand_overview where
start_date = = concat(SUBSTRING(PARAM_CURRENT_MONTH_START_DATE:,1,4)+'-' , concat(SUBSTRING(PARAM_CURRENT_MONTH_START_DATE:,5,2)+'-' , SUBSTRING(PARAM_CURRENT_MONTH_START_DATE:,7,2)))
and end_date = concat(SUBSTRING(PARAM_CURRENT_MONTH_END_DATE:,1,4)+'-' , concat(SUBSTRING(PARAM_CURRENT_MONTH_END_DATE:,5,2)+'-' , SUBSTRING(PARAM_CURRENT_MONTH_END_DATE:,7,2)));
      
