select total_unique_cards as current_spend from onyx.monthly_brand_overview a
join report_info b on a.yyyymm = b.report_month
where a.start_date = cast (cast(PARAM_PRECEDING_MONTH_START_DATE: as varchar(10)) as date)
and a.end_date = cast (cast(PARAM_PRECEDING_MONTH_END_DATE: as varchar(10)) as date)
and
uid='%s';
