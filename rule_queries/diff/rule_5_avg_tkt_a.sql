select 100*((curr_avg-prev_avg)/prev_avg) from
(select avg_tkt as curr_avg from monthly_brand_overview a join report_info b
on a.YYYYMM = b.report_month and a.start_date = b.param_current_month_start_date
where uid = '%s'
) c ,
(select avg_tkt as prev_avg from monthly_brand_overview a join report_info b
on a.YYYYMM = b.report_month
where a.start_date = PARAM_PRECEDING_MONTH_START_DATE: and uid = '%s' ) d;
