-- Rule 15:-
-- Unique = Return + new for current month
-- q1+q2=q3

-- query 1 = select uid, SUM(cards) from onyx_app_tc_customer_status_monthly where card_status = 'Return' and year = '2016' and month = '9' group by uid order by uid;
-- query 2 = select uid, SUM(cards) from onyx_app_tc_customer_status_monthly where card_status = 'New' and year = '2016' and month = '9' group by uid order by uid;
-- query 3 = select uid, unique_cards from monthly_brand_overview where start_date = '2016-09-01' and end_date = '2016-09-30' order by uid;



select unique_cards from PARAM_RS_ONYX_SCHEMA:.monthly_brand_overview where 
start_date = = concat(SUBSTRING(PARAM_PREV_YEAR_CURRENT_MONTH_START_DATE:,1,4)+'-' , concat(SUBSTRING(PARAM_PREV_YEAR_CURRENT_MONTH_START_DATE:,5,2)+'-' , SUBSTRING(PARAM_PREV_YEAR_CURRENT_MONTH_START_DATE:,7,2)))
and end_date = concat(SUBSTRING(PARAM_PREV_YEAR_CURRENT_MONTH_END_DATE:,1,4)+'-' , concat(SUBSTRING(PARAM_PREV_YEAR_CURRENT_MONTH_END_DATE:,5,2)+'-' , SUBSTRING(PARAM_PREV_YEAR_CURRENT_MONTH_END_DATE:,7,2)))
and uid=%s; 



