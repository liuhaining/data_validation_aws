select (total_unique_cards/active_stores) as avg_unique from PARAM_RS_ONYX_SCHEMA:.monthly_brand_overview
a join report_info b
on a.yyyymm = b.report_month where a.start_date = cast (cast(PARAM_PREV_YEAR_CURRENT_MONTH_START_DATE: as varchar(10)) as date)
and a.end_date = cast (cast(PARAM_PREV_YEAR_CURRENT_MONTH_END_DATE: as varchar(10)) as date)
and uid='%s';

