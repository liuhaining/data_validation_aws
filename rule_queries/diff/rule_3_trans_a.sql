select 100*((curr_txns-prev_txns)/prev_txns) from
(select txns::NUMERIC(10,2) as curr_txns from monthly_brand_overview a join report_info b
on a.YYYYMM = b.report_month and a.start_date = b.param_current_month_start_date
where uid = '%s'
) c ,
(select txns::NUMERIC(10,2) as prev_txns from monthly_brand_overview a join report_info b
on a.YYYYMM = b.report_month
where a.start_date = PARAM_PRECEDING_MONTH_START_DATE: and uid = '%s' ) d;

