-- Rule 12:-sale in monthly_store_sales = sales in monthly_brand_overview for current month all brands.

/*q1 = select uid, SUM(sale) from nya_joe_restaurant.monthly_store_sales_growth where year=2016 and month=9 group by uid;
 

q2 = select uid, sale from nya_joe_restaurant.monthly_brand_overview where start_date = '2016-09-01' and end_date = '2016-09-30';

q1 = q2
*/
select uid, sale from %s.monthly_brand_overview where 
start_date = = concat(SUBSTRING(PARAM_CURRENT_MONTH_START_DATE:,1,4)+'-' , concat(SUBSTRING(PARAM_CURRENT_MONTH_START_DATE:,5,2)+'-' , SUBSTRING(PARAM_CURRENT_MONTH_START_DATE:,7,2)))
and end_date = concat(SUBSTRING(PARAM_CURRENT_MONTH_END_DATE:,1,4)+'-' , concat(SUBSTRING(PARAM_CURRENT_MONTH_END_DATE:,5,2)+'-' , SUBSTRING(PARAM_CURRENT_MONTH_END_DATE:,7,2)));
       