-- Rule 4 :- Avg ticket size current month using monthly brand table and weekly brand table

-- for current month all brands.

/*
q1 = select uid, AVG(avg_tkt) from nya_joe_restaurant.weekly_brand_performance_trend where year=2016 and week_no in ('36', '37', '38') group by uid;

q2 = select avg_tkt from nya_joe_restaurant.monthly_brand_overview where start_date = '2016-09-01' and end_date = '2016-09-30';

q1 ~ q2 (almost equal. Can be 1-2% different because of the beginning and ending of month have few extra days considered in the monthly view. I am keeping a smaller window for comparison trying to get apple-apple comparison, and avoiding spikes typically at the beginning/ending of month.)
*/

select uid, AVG(avg_tkt) from %s.weekly_brand_performance_trend where year=PARAM_CURRENT_YEAR: and week_no in ('36', '37', '38') group by uid;
     