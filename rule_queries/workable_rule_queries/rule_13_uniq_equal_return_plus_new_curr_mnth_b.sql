select uid, unique_cards from monthly_brand_overview 
where start_date = cast (cast(PARAM_CURRENT_MONTH_START_DATE: as varchar(10)) as date)
and end_date = cast (cast(PARAM_CURRENT_MONTH_END_DATE: as varchar(10)) as date)
and uid = %s
