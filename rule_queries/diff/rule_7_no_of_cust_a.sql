select 100*((curr_tot_customers-prev_tot_customers)/prev_tot_customers) from
(select total_unique_cards::NUMERIC(10,2) as curr_tot_customers from monthly_brand_overview a join report_info b
on a.YYYYMM = b.report_month and a.start_date = b.param_current_month_start_date
where uid = '%s'
) c ,
(select  total_unique_cards::NUMERIC(10,2) as prev_tot_customers from monthly_brand_overview a join report_info b
on a.YYYYMM = b.report_month
where a.start_date = PARAM_PRECEDING_MONTH_START_DATE: and uid = '%s' ) d;

