-- Rule 11:-
-- target_spend in onyx_app_tc_customer_status is same as spend for same month in monthly_brand_overview

-- query 1 :- select uid, sale as current_spend from monthly_brand_overview where start_date ='2016-09-01' and end_date='2016-09-30' group by uid, sale order by uid;

-- same as

-- query 2 :- select uid, SUM(spend) as target_spend from onyx_app_tc_customer_status_monthly where month = 9 and year = 2016 group by uid order by uid;



 select SUM(spend) as target_spend from PARAM_RS_ONYX_SCHEMA:.onyx_app_tc_customer_status_monthly where month = PARAM_CURRENT_MONTH: and year = PARAM_CURRENT_YEAR: and uid=%s  group by uid;
       