-- Rule 11:-
-- target_spend in onyx_app_tc_customer_status is same as spend for same month in monthly_brand_overview

-- query 1 :- select uid, sale as current_spend from monthly_brand_overview where start_date ='2016-09-01' and end_date='2016-09-30' group by uid, sale order by uid;

-- same as

-- query 2 :- select uid, SUM(spend) as target_spend from onyx_app_tc_customer_status_monthly where month = 9 and year = 2016 group by uid order by uid;



 select sale as current_spend from PARAM_RS_ONYX_SCHEMA:.monthly_brand_overview where 
 start_date = = concat(SUBSTRING(PARAM_CURRENT_MONTH_START_DATE:,1,4)+'-' , concat(SUBSTRING(PARAM_CURRENT_MONTH_START_DATE:,5,2)+'-' , SUBSTRING(PARAM_CURRENT_MONTH_START_DATE:,7,2)))
and end_date = concat(SUBSTRING(PARAM_CURRENT_MONTH_END_DATE:,1,4)+'-' , concat(SUBSTRING(PARAM_CURRENT_MONTH_END_DATE:,5,2)+'-' , SUBSTRING(PARAM_CURRENT_MONTH_END_DATE:,7,2))) 
 and uid=%s  group by uid, sale;
     
