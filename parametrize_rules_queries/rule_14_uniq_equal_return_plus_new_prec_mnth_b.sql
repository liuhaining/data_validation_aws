-- Rule 14:-
-- Unique = Return + new for current month
-- q1+q2=q3

-- query 1 = select uid, SUM(cards) from onyx_app_tc_customer_status_monthly where card_status = 'Return' and year = '2016' and month = '9' group by uid order by uid;
-- query 2 = select uid, SUM(cards) from onyx_app_tc_customer_status_monthly where card_status = 'New' and year = '2016' and month = '9' group by uid order by uid;
-- query 3 = select uid, unique_cards from monthly_brand_overview where start_date = '2016-09-01' and end_date = '2016-09-30' order by uid;



select SUM(cards) from PARAM_RS_ONYX_SCHEMA:.onyx_app_tc_customer_status_monthly where card_status = 'New' and year = PARAM_CURRENT_YEAR: and month = PARAM_PREV_MONTH: and uid=%s  group by uid;
     



