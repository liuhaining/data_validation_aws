-- Rule 8:-
-- Avg ticket size < avg spend at your locations current month
-- query 1 :- select uid, avg_tkt from monthly_brand_overview where start_date ='2016-09-01' and end_date='2016-09-30'  group by uid, avg_tkt order by uid, avg_tkt;

-- always less than or equal to 

-- query 2 :- select uid, sale/unique_cards as avg_spend_at_loc from monthly_brand_overview where start_date ='2016-09-01' and end_date='2016-09-30' group by uid, start_date, sale, unique_cards, avg_tkt order by avg_tkt, start_date;



select avg_tkt from PARAM_RS_ONYX_SCHEMA:.monthly_brand_overview where 
start_date = = concat(SUBSTRING(PARAM_CURRENT_MONTH_START_DATE:,1,4)+'-' , concat(SUBSTRING(PARAM_CURRENT_MONTH_START_DATE:,5,2)+'-' , SUBSTRING(PARAM_CURRENT_MONTH_START_DATE:,7,2)))
and end_date = concat(SUBSTRING(PARAM_CURRENT_MONTH_END_DATE:,1,4)+'-' , concat(SUBSTRING(PARAM_CURRENT_MONTH_END_DATE:,5,2)+'-' , SUBSTRING(PARAM_CURRENT_MONTH_END_DATE:,7,2))) 
and uid=%s;  
     