select 100*((curr_sales-prev_sales)/prev_sales) from
(select sale as curr_sales from monthly_brand_overview a join report_info b
on a.YYYYMM = b.report_month and a.start_date = b.param_current_month_start_date
where uid = '%s'
) c ,
(select sale as prev_sales from monthly_brand_overview a join report_info b
on a.YYYYMM = b.report_month 
where a.start_date = PARAM_PRECEDING_MONTH_START_DATE: and uid = '%s' ) d;
