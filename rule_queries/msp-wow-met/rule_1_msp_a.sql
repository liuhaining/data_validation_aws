select trunc((tg_metrics.tg_met-cg_metrics.cg_met),2) as msp from 
( 
	select tg_curr_mon.uid,(tg_curr_mon.ts_curr_month-tg_prev_mon.ts_prev_month)/tg_prev_mon.ts_prev_month as tg_met from 
	( 
	select uid,sum(spend) as ts_curr_month from staging.onyx_app_tc_customer_status_monthly where uid = '%s' and year = PARAM_CURRENT_YEAR: and month = PARAM_CURRENT_MONTH: group by uid
	)tg_curr_mon,
	(
	select uid,sum(spend) as ts_prev_month from staging.onyx_app_tc_customer_status_monthly where uid = '%s' and year = PARAM_CURRENT_YEAR: and month = PARAM_PREV_MONTH: group by uid
	) tg_prev_mon
	where tg_curr_mon.uid = tg_prev_mon.uid
) tg_metrics,
(
	select cg_prev_mon.uid,(cg_curr_mon.cs_curr_month-cg_prev_mon.cs_prev_month)/cg_prev_mon.cs_prev_month as cg_met from
	( 
	select uid,sum(cohort_spend) as cs_curr_month from staging.onyx_app_tc_customer_status_monthly where uid = '%s' and year = PARAM_CURRENT_YEAR: and month = PARAM_CURRENT_MONTH: group by uid
	)cg_curr_mon,
	(
	select uid,sum(cohort_spend) as cs_prev_month from staging.onyx_app_tc_customer_status_monthly where uid = '%s' and year = PARAM_CURRENT_YEAR: and month = PARAM_PREV_MONTH: group by uid
	) cg_prev_mon
	where cg_curr_mon.uid = cg_prev_mon.uid
) cg_metrics
where tg_metrics.uid = cg_metrics.uid;
