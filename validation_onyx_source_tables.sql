
--VALIDATION : New table generated in HIVE onyx database, FORMAT - ORC 
-- DATE : 2016-10-07

--------------------****** onyx.cohort ***-------------------------------
  hive (onyx)> select * from cohort_old where target_mid is null;
OK
cohort_old.uid  cohort_old.target_mid   cohort_old.target_pid   cohort_old.cohort_mid   cohort_old.cohort_pid
uid     NULL    NULL    NULL    NULL
(76448 rows)    NULL    NULL    NULL    NULL
Time taken: 0.284 seconds, Fetched: 2 row(s)


 hive (onyx)> select * from cohort where target_mid is null;
OK
cohort.uid      cohort.target_mid       cohort.target_pid       cohort.cohort_mid       cohort.cohort_pid
uid     NULL    NULL    NULL    NULL
(76448 rows)    NULL    NULL    NULL    NULL
Time taken: 0.444 seconds, Fetched: 2 row(s)


hive (onyx)> select distinct uid from cohort limit 10;

OK
uid
(76448 rows)
AllenEdmonds_clothing
BuffaloWildWings_restaurant
CheapTobacco_convenience
ChristianLouboutin_clothing
CityBBQ_convenience
CityTapHouse_bar
Dishes_retail
DonatosPizzeria_pizza
EssexTechnologyGroup/BargainHunt_electronics
Time taken: 12.45 seconds, Fetched: 10 row(s)
hive (onyx)> select count(*) from cohort;

76450
Time taken: 12.43 seconds, Fetched: 1 row(s)

hive (onyx)> select * from cohort limit 10;
OK
cohort.uid      cohort.target_mid       cohort.target_pid       cohort.cohort_mid       cohort.cohort_pid
uid     NULL    NULL    NULL    NULL
sbarro  1827483847189757791     1000    3858123653508002290     1020
sbarro  1827483847189757791     1000    3669437689834440201     1020
sbarro  1827483847189757791     1000    5022886270815591243     1020
sbarro  1827483847189757791     1000    2032566487447544265     1020
sbarro  1827483847189757791     1000    2281372408656192846     1000
sbarro  1827483847189757791     1000    945638363423110687      1000
sbarro  1827483847189757791     1000    2266891441450015949     1000
sbarro  1827483847189757791     1000    9129433617803392533     1000
sbarro  1827483847189757791     1000    7803963822042578540     1000
Time taken: 0.341 seconds, Fetched: 10 row(s)

--------------------****** onyx.daily_aggr ***-------------------------------

hive (onyx)> select count(*) from onyx.daily_aggr;

OK
_c0
16892510
Time taken: 12.341 seconds, Fetched: 1 row(s)

hive (onyx)> select * from daily_aggr limit 10;
OK
daily_aggr.mid  daily_aggr.pid  daily_aggr.date daily_aggr.spend        daily_aggr.trans
11586899862600  1000    20160810        671.75  6
67749104439828  1000    20160805        2110.0  14
330373919023781 1030    20160806        450.0   3
381448250821048 1030    20160801        199.71  2
392078327087213 1000    20160806        528.97  21
512887582713366 1000    20160809        1505.4  2
587240523122235 1030    20160810        1576.95 2
1504115847125619        1040    20160806        5949.07 132
1597499102161185        1040    20160801        352.97  25
2568070449008508        1000    20160806        137.0   2
Time taken: 0.315 seconds, Fetched: 10 row(s)


hive (onyx)> select distinct pid from onyx.daily_aggr;
OK
pid
1000
1030
1010
1020
1040
Time taken: 12.391 seconds, Fetched: 5 row(s)

hive (onyx)> select * from titan.odnm WHERE y=2016 and m=8 and d=20160801 and mid=25825385405903397 limit 50;
odnm.mcc        odnm.mid        odnm.pid        odnm.card       odnm.amount     odnm.type       odnm.dma        odnm.gid        odnm.zip        odnm.h  odnm.y  odnm.m odnm.d
7538    25825385405903397       1000    3000932323502443372     93.5    2       803     39115   91016   17      2016    8       20160801
7538    25825385405903397       1000    5936778506393593439     120     2       803     39115   91016   22      2016    8       20160801
7538    25825385405903397       1000    389528026472301694      92      2       803     39115   91016   21      2016    8       20160801
7538    25825385405903397       1000    389528026472301694      95.88   2       803     39115   91016   22      2016    8       20160801
7538    25825385405903397       1000    6665169052787162622     75      2       803     39115   91016   13      2016    8       20160801
7538    25825385405903397       1000    3752605413479751018     113.36  2       803     39115   91016   13      2016    8       20160801
7538    25825385405903397       1000    7379980191575805622     73.98   2       803     39115   91016   16      2016    8       20160801
Time taken: 42.332 seconds, Fetched: 7 row(s)

hive (onyx)> select mid,pid,CONCAT(SUBSTR(d,1,4), '-',SUBSTR(d,5,2), '-',SUBSTR(d,7,2)) dt,SUM(amount),COUNT(card) from titan.odnm WHERE y=2016 and m=8 and d=20160801 and mid=25825385405903397 group by mid,pid,d;

mid     pid     dt      _c3     _c4
25825385405903397       1000    2016-08-01      663.72  7
Time taken: 34.369 seconds, Fetched: 1 row(s)

hive (onyx)> select * from daily_aggr_old WHERE date='2016-08-01' limit 40;
OK
daily_aggr.mid  daily_aggr.pid  daily_aggr.date daily_aggr.spend        daily_aggr.trans
4176255535044547362     1000    2016-08-01      2883.18 114
8653381507798514440     1000    2016-08-01      595.53  24
25825385405903397       1000    2016-08-01      663.72  7
.
.
....

hive (onyx)> select * from daily_aggr WHERE date=20160801 and mid in(4176255535044547362,8653381507798514440,25825385405903397);
OK
daily_aggr.mid  daily_aggr.pid  daily_aggr.date daily_aggr.spend        daily_aggr.trans
8653381507798514440     1000    20160801        595.53  24
25825385405903397       1000    20160801        663.72  7
4176255535044547362     1000    20160801        2883.18 114
Time taken: 0.231 seconds, Fetched: 3 row(s)

--------------------****** onyx.target_trans ***-------------------------------

hive (onyx)> EXPLAIN select distinct date from target_trans tn limit 10;
Execution mode: vectorized

hive (onyx)> select distinct date from target_trans ;
--ERROR : Status: Failed
Vertex failed, vertexName=Map 1, vertexId=vertex_1474524320269_3855_3_00
, diagnostics=[Task failed, taskId=task_1474524320269_3855_3_00_000001, 
diagnostics=[TaskAttempt 0 failed, 
info=[Error: Failure while running task:java.lang.RuntimeException: org.apache.hadoop.hive.ql.metadata.HiveException:
 java.io.IOException: java.lang.RuntimeException: java.lang.ClassCastException: 
 org.apache.hadoop.hive.common.type.HiveVarchar cannot be cast to java.lang.String


hive (onyx)> set hive.vectorized.execution.enabled=false;

FIX found from URL : https://discuss.pivotal.io/hc/en-us/articles/221253727-Hive-query-fails-with-HiveVarchar-cannot-be-cast-to-java-lang-String-
JIRA TICKET FOR THE FIX: https://issues.apache.org/jira/browse/HIVE-11054

THE other fix is to change the PARTITION to STRING type instead of VARCHAR

hive (onyx)> select * from target_trans limit 10;
OK
target_trans.mid        target_trans.pid        target_trans.h  target_trans.date       target_trans.amount     target_trans.card       target_trans.uid
8694278854764653534     1000    19      20160801        371.0   7871116736839066968     AllenEdmonds_clothing
8694278854764653534     1000    17      20160801        190.8   5151509229384610233     AllenEdmonds_clothing
9095133234110707271     1000    17      20160801        440.57  3529356483046994885     AllenEdmonds_clothing
9095133234110707271     1000    22      20160801        437.38  8339886992642109192     AllenEdmonds_clothing
8260748785670721023     1000    21      20160801        9.77    5099272254867460377     AllenEdmonds_clothing
6387887953583815623     1000    21      20160801        335.48  967426172131421338      AllenEdmonds_clothing
6244162021280734742     1000    18      20160801        153.7   7111047650701809129     AllenEdmonds_clothing
4021322677739755620     1000    20      20160801        407.77  678259231964535789      AllenEdmonds_clothing
3335238398069546322     1000    19      20160801        416.19  5140222876870423672     AllenEdmonds_clothing
2511272286040047590     1000    17      20160801        324.5   2316628212885931765     AllenEdmonds_clothing
Time taken: 0.295 seconds, Fetched: 10 row(s)


hive (onyx)> select distinct date from target_trans ;

OK
date
20160801
20160802
20160803
20160804
20160805
20160806
20160807
20160808
20160809
20160810
Time taken: 14.346 seconds, Fetched: 10 row(s)

hive (onyx)> select count(*) From target_trans tn;
OK
_c0
4216211
Time taken: 14.347 seconds, Fetched: 1 row(s)

hive (onyx)> select date, count(*) from target_trans group by date;
OK
date    _c1
20160801        375346
20160802        397384
20160803        404772
20160804        427730
20160805        504101
20160806        515008
20160807        426384
20160808        367638
20160809        392180
20160810        405668
Time taken: 22.347 seconds, Fetched: 10 row(s)


--------------------****** onyx.cohort_trans ***-------------------------------


hive (onyx)> select * from cohort_trans ct limit 10;
OK
ct.target_mid   ct.target_pid   ct.cohort_mid   ct.cohort_pid   ct.mcc  ct.card ct.amount       ct.zip  ct.y    ct.m    ct.d    ct.uid
8694278854764653534     1000    307549797365743470      1000    5611    5064857799977494741     130     48203   2016    8       20160801        AllenEdmonds_clothing
5416768376053807931     1000    236582702924991627      1000    5691    8548733742058546699     31.54   94102   2016    8       20160801        AllenEdmonds_clothing
8375442036649472550     1000    236582702924991627      1000    5691    8548733742058546699     31.54   94102   2016    8       20160801        AllenEdmonds_clothing
5416768376053807931     1000    236582702924991627      1000    5691    3361776233423968972     310.19  94102   2016    8       20160801        AllenEdmonds_clothing
8375442036649472550     1000    236582702924991627      1000    5691    3361776233423968972     310.19  94102   2016    8       20160801        AllenEdmonds_clothing
3335238398069546322     1000    244936168758665752      1000    5651    8526851838496628849     49.5    2199    2016    8       20160801        AllenEdmonds_clothing
9095133234110707271     1000    244936168758665752      1000    5651    8526851838496628849     49.5    2199    2016    8       20160801        AllenEdmonds_clothing
6024768783516163971     1000    244936168758665752      1000    5651    8526851838496628849     49.5    2199    2016    8       20160801        AllenEdmonds_clothing
1919855560215746219     1000    251908487746647893      1030    5137    2948451879303344956     37.11   37027   2016    8       20160801        AllenEdmonds_clothing
1919855560215746219     1000    251908487746647893      1030    5137    8955330856402200279     92.74   37027   2016    8       20160801        AllenEdmonds_clothing
Time taken: 0.343 seconds, Fetched: 10 row(s)

hive (onyx)> select distinct cohort_pid from cohort_trans ct limit 10;
OK
cohort_pid
1030
1040
1000
1010
1020
Time taken: 26.371 seconds, Fetched: 5 row(s)

hive (onyx)> select d, count(*) from cohort_trans group by d;
OK
d       _c1
20160802        9109024
20160807        9414091
20160810        9641988
20160805        11223921
20160804        9983073
20160808        8924015
20160806        10966049
20160801        8966406
20160803        9545369
20160809        9255968
Time taken: 48.356 seconds, Fetched: 10 row(s)

hive (onyx)> select count(*) from cohort_trans;

OK
_c0
97029904
Time taken: 18.341 seconds, Fetched: 1 row(s)

