select sum(c) from 
(select SUM(cards) as c from staging.onyx_app_tc_customer_status_monthly 
where uid = %s and card_status = 'Return' and year = PARAM_CURRENT_YEAR: and month = PARAM_CURRENT_MONTH: 
UNION ALL 
select SUM(cards) as c from staging.onyx_app_tc_customer_status_monthly 
where uid = %s and card_status = 'New' and year = PARAM_CURRENT_YEAR: and month = PARAM_CURRENT_MONTH:) a; 
