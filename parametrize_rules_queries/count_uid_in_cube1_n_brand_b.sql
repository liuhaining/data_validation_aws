-- Unit test
-- Distinct uid in daily_aggr_tc_zip is same as brand table
-- query 1 :- select COUNT(DISTINCT(uid)) from daily_aggr_tc_zip;

-- SAME as 

-- query 1 :- select COUNT(DISTINCT(uid)) from brand;


select COUNT(DISTINCT(uid)) from PARAM_RS_ONYX_SCHEMA:.brand;