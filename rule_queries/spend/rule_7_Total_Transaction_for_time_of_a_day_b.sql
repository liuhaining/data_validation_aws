select SUM(txns) from onyx.customer_activity_by_time a join report_info b on (concat(a.year,case when length(a.month)=1 then 0||a.month else a.month||'' end)) = b.report_month
where year=PARAM_CURRENT_YEAR: and month=PARAM_CURRENT_MONTH: and time_frame='time_of_a_day' and uid='%s';
