select sum(c) from
(select SUM(cards) as c from staging.onyx_app_tc_customer_status_monthly
where uid = 'modern_market' and card_status = 'Return' and month = PARAM_PREV_MONTH:
AND
CASE 
    When month = 12 then year = PARAM_PREV_YEAR: Else year = PARAM_CURRENT_YEAR:
END
UNION ALL
select SUM(cards) as c from staging.onyx_app_tc_customer_status_monthly
where uid = 'modern_market' and card_status = 'New' and  month = PARAM_PREV_MONTH:
AND
CASE 
    When month = 12 then year = PARAM_PREV_YEAR: Else year = PARAM_CURRENT_YEAR:
END
	) a;
