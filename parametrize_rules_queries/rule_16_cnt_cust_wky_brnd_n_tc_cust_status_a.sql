/* 
Rule - 16
count of customers from weekly_brand_performance_trend <= count of customers at onyx_app_tc_customer_status which is at store level for the exact same weeks of the year.

Brand level should be <= store level.

This is because when the stores are close the customer is considered returning for the brand.

Threshold of difference needs to be built per brand. */

/*
q1 = select uid, SUM(unique_cards) from nya_joe_restaurant.weekly_brand_performance_trend where year='2016' and week_no IN ('35', '36', '37', '38', '39') group by uid;

q2 = select uid, SUM(cards) from onyx_app_tc_customer_status_weekly where uid = 'nya_joe_restaurant' and year='2016' and week IN ('35', '36', '37', '38', '39') group by uid;

*/

 
select uid, SUM(unique_cards) from %s.weekly_brand_performance_trend where year=PARAM_CURRENT_YEAR: and week_no IN ('35', '36', '37', '38', '39') group by uid;
     
        
		
