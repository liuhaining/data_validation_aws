SELECT a.diff 
FROM   ( 
                SELECT   mid, 
                         value, 
                         ranking, 
                         patient_zip, 
                         store_zip, 
                         (store_zip-patient_zip) AS diff 
                FROM     monthly_store_geo_stats 
                WHERE    mid IN 
                                 ( 
                                 SELECT DISTINCT mid 
                                 FROM            monthly_store_geo_stats 
                                 WHERE           uid LIKE '%s' 
                                 AND             year =PARAM_CURRENT_YEAR: 
                                 and             month =PARAM_CURRENT_MONTH: 
                                 and             type_of_result = 'home') 
                AND      uid LIKE '%s' 
                AND      year = param_current_year 
                AND      month = param_current_month 
                AND      type_of_result = 'home' 
                ORDER BY ranking ASC)a 
WHERE  a.diff > 10000 limit 1;