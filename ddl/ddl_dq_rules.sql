use workflow;

CREATE TABLE `onyx_dq_rules` (
  `rule_id` int(11) NOT NULL DEFAULT '0',
  `query_path1` varchar(200) DEFAULT NULL,
  `query_path2` varchar(200) DEFAULT NULL,
  `operator` varchar(4) DEFAULT NULL,
  `tolerance` decimal(12,2) DEFAULT NULL,
  `comments` varchar(40) DEFAULT NULL,
  `is_active` int(3) DEFAULT NULL 
); 
