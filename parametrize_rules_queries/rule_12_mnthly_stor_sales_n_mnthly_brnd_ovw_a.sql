-- Rule 12:- sale in monthly_store_sales = sales in monthly_brand_overview for current month all brands.

/*q1 = select uid, SUM(sale) from nya_joe_restaurant.monthly_store_sales_growth where year=2016 and month=9 group by uid;
 

q2 = select uid, sale from nya_joe_restaurant.monthly_brand_overview where start_date = '2016-09-01' and end_date = '2016-09-30';

q1 = q2
*/
select uid, SUM(sale) from %s.monthly_store_sales_growth where year=PARAM_CURRENT_YEAR: and month=PARAM_CURRENT_MONTH: group by uid;
    