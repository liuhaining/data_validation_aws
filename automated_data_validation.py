from __future__ import division
import os
import psycopg2
import pymysql
import datetime
import ConfigParser
import re
import sharedlib.utils as utils
import traceback
import sys
import email_dq_results
import report_bck_ddl

def substitute_query_params_new(query_file_path, uid, param_file_path):

    # The function replace the parameter with the values in the queries and return the runnable query

    try:
        query_list = []
        for path in query_file_path:
	  #  import pdb;pdb.set_trace()
            with open(path, 'r') as sql_file:
                data=sql_file.read()
            if  param_file_path !=0:
                with open(param_file_path, 'r') as param_file:
                    param_result = {}
                    for line in param_file:
                        li=line.strip()
                        if not li.startswith("["):
                            k, v = li.split(' ')
                            param_result[k.strip()] = v.strip()
                    data_dict = dict(param_result)
                    query=data
                    data_list = re.findall(r'PARAM_[^:]+:',data)
                    uid_list = re.findall(r'%s',data)
                    if data_list:
                        for param in data_list:
                            value = param
                            key_value = data_dict.get(value)
                            if key_value is not None:
                                query = query.replace(param,data_dict[value])
		    if uid_list:
		        for id in uid_list:
		            query = query.replace(id,uid) 
		        query_list.append(query)
                    else:
			query_list.append(query)
			logger.info("uid is not present in the query")
                   
            else:
		pass
        return query_list
           
    except Exception as e:
	logger.info(str(traceback.format_exc()))
	raise

def update_dq_row_status(config,rs_onyx_conn,rs_onyx_cur, uid, is_data_quality):
 
	# This function updates the status of execution at each stage in pdf_gen_result_matrix table
	# 1 for in process, 2 for success, 3 for failure
      
	try:
		process_update_qry = """
		UPDATE %s.%s 
		SET is_data_quality=%s
		WHERE uid='%s';
		"""%(config.get('redshift','rs_ops_schema'),config.get('redshift','onyx_pdf_gen_matrix_tbl'),is_data_quality, uid) 
		logger.info("process update query:"+str(process_update_qry))
		process_update_qry = """
                UPDATE %s.%s
                SET is_data_quality=%s
                WHERE uid='%s';
                """%(config.get('redshift','rs_ops_schema'),config.get('redshift','onyx_pdf_gen_matrix_tbl'),is_data_quality, uid)
                logger.info("process update query:"+str(process_update_qry))

		rs_onyx_cur.execute(process_update_qry)
		rs_onyx_conn.commit()
		return 0 
	except Exception as e:
		logger.info(str(traceback.format_exc()))
		raise

def execute(project, project_batch, batch_id, param_file_path):
        # This function will fetch the rules(test cases) from rules table(MySQL)
        # Replaces the query parameters in the query(testcase) file
	# fetch the brands from pdf_gen_result_matrix
	# Execute the queries for each brand and validates the result
	# stores the result in onyx_dq_rule_result
	# updates the result for uid in pdf_gen_result_matrix

	logger.info("\t Script started. Redshift and MySQL data bases are connected")
	test_cases_qry = 'SELECT * from %s where rule_id between 30 and 38'%config.get('mysql', 'onyx_dq_rule_tbl')
	rules = mysql.execute(test_cases_qry);
	rules = mysql.fetchall()
	try:
		if rules:
	#		import pdb; pdb.set_trace()
			logger.info("Rules query: "+str(test_cases_qry)+'\n')
			logger.info("TEST CASES: "+str(rules)+'\n')
                	brand_name_qry = """SELECT DISTINCT uid from %s.%s where is_view_created = %s and project = '%s' and project_batch = '%s' and batch_id = %s order by id"""%(config.get('redshift','rs_ops_schema'),config.get('redshift', 'onyx_pdf_gen_matrix_tbl'),2,project,project_batch,batch_id)
			logger.info("Brand query "+str(brand_name_qry))
                	brand_name = rs_onyx_cur.execute(brand_name_qry)
			brand = rs_onyx_cur.fetchall()
			if brand:
                		logger.info("Brands qualified for data validation:\t"+str(brand)+'\n')
				brand_list = [brand[0] for brand in brand]
				uid_results = {}
				for uid in brand_list:
                        		result_list = []
					update_dq_row_status(config,rs_onyx_conn, rs_onyx_cur, uid, is_data_quality=1)
					brand_status_qry = """SELECT is_uid_active from onyx.brand_operation where uid = '%s'"""%(uid)
					brand_status = rs_onyx_cur.execute(brand_status_qry)
				        brand_status = rs_onyx_cur.fetchall()
					if len(brand_status) ==1 and brand_status[0][0] == 't':
						rules_result = {}
						for ruleid in rules:
					#		import pdb; pdb.set_trace()
							if ruleid['is_active']:
								newlist = [ruleid[filename] for filename in ruleid.keys() if filename in ('query_path1', 'query_path2')]
								rule_tolerance = ruleid['tolerance'] if ruleid['tolerance'] is not None else 0
 								operator = ruleid['operator']
								comments = ruleid['comments']
								ruleid = ruleid['rule_id']
								validation_list = []
								query_list = substitute_query_params_new(newlist, uid, param_file_path)
								for query in query_list:
									try:
										print query.replace('\n',' ')
										result = rs_onyx_cur.execute(query.replace('\n',' '))
										final = rs_onyx_cur.fetchall()
										if len(final) >= 1:
											final = final[0][0]
                                                					logger.info('uid: '+str(uid)+' '+'ruleid: '+str(ruleid)+' '+'query: '+str(query)+' '+'result:'+str(final))
											if not isinstance(final,type(None)):
												validation_list.append(float(final))
											else:
												validation_list.append(str(final))
										else:
											if operator == 'D':
												operator = "=="
												final = 0.0
											else:
												final = "EMPTY"
											validation_list.append(final)
									except Exception as e:
										logger.info(str(traceback.format_exc()))
										rs_onyx_conn.commit()
										continue
								testquery = [(str.replace(item,"'","\\'")) for item in query_list]
								if len(validation_list) == 2 and 'None' not in validation_list and 'EMPTY' not in validation_list: 
                                					logger.info("Results of query1 and query2\t"+str(validation_list))
									logger.info("RuleID:"+str(ruleid)+' '+'Operator:'+str(operator))
									if eval("validation_list[0] %s validation_list[1]"%(operator)):
                                						result = int(True)
										difference = 0
										comments = ''
                                        					logger.info('UID :'+str(uid)+ 'Ruleid: '+str(ruleid)+' '+'PASS')
									else:
										value1 = validation_list[0]
        		                                                	value2 = validation_list[1]
										if not (isinstance(value1, str) and isinstance(value2,str)):	
											difference = validation_list[0] - validation_list[1]
											if difference and rule_tolerance: 
												try:
                                                                                			actual_tolerance = round(abs((validation_list[1] - validation_list[0])/validation_list[0]),1)
                                                                                			logger.info("Rule_tolerance"+str(actual_tolerance))
                                                                                			logger.info("Rule_tolerance"+str(rule_tolerance))
                                                                        			except ZeroDivisionError as e:
                                                                                			if 'integer division or modulo by zero' in e.message:
                                                                                        			result = int(False)
                                                                        			if actual_tolerance <= float(rule_tolerance):
                                                                                			result = int(True)
                                                                        			else:
                                                                                			result = int(False)
                                                                        			comments = ""
                                                                			else:
                                                                        			result = int(False)
                                                                        			comments = "Exact match not found"
                                                                			logger.info(str(batch_id)+ 'Ruleid: '+str(ruleid)+' '+str(result))
                                                        			else:
                                                                			difference = set(value1)-set(value2)
                                                                			result = int(False)
                                                                			comments = ""	
								elif validation_list.count('EMPTY') == 1:
                                                			difference = 0
                                                			actual_tolerance = 0
                                                			result = int(False)
                                                			comments = "Data not found in the table for a query"
                                                			logger.info(str(ruleid)+": One of the rule has no data")
                                        			elif validation_list.count('EMPTY') == 2:
                                                			difference = 0
                                                			actual_tolerance = 0
                                                			result = int(False)
                                                			logger.info("Ruleid:"+str(ruleid)+": Both the rules has no data")
                                                			comments = "Data not found in the table for both the queries. Test case failed"
                                        			elif 'error' in validation_list:
                                                			difference = 0
                                                			actual_tolerance = 0
                                                			result = int(False)
                                                			logger.info(str(ruleid)+": Error occured for the test queries")
                                                			comments = "Rule execution failed. Error in executing test case query"
								elif "None" in validation_list:
									difference = 0
									actual_tolerance = 0
									result = int(False)
									logger.info(str(ruleid)+": No result found for the query")
									comments = "No result found for the query"
								result_list.append(result)
								rules_result[ruleid] = bool(result)
								
								result_table_ins_qry = """INSERT INTO %s.%s (batch_id,uid,run_date, ruleid,query_1, query_2,query_1_result,query_2_result,difference,operator, result,comments)VALUES(%s,'%s','%s',%s,'%s','%s','%s','%s','%s','%s',%s,'%s');"""%(config.get('redshift','rs_ops_schema'),config.get('redshift', 'onyx_dq_rule_result_tbl'),batch_id,uid,datetime.datetime.now(),ruleid,testquery[0].strip('\r'),testquery[1].strip('\r'),validation_list[0],validation_list[1],difference,operator,result,comments)
								insert_query = rs_onyx_cur.execute(result_table_ins_qry)
                                				if 0 in result_list:
									update_dq_row_status(config,rs_onyx_conn, rs_onyx_cur, uid, is_data_quality=3)
                                				else:
									update_dq_row_status(config,rs_onyx_conn, rs_onyx_cur, uid, is_data_quality=2)
                                					logger.info("DQ status updated")
							else:
								print "run flag zero is set for ruleid : %s"%(ruleid['rule_id'])
					else:
						rules_result = {}
						comments = 'Brand operation status having status F'
						result_table_ins_qry = """INSERT INTO %s.%s (batch_id,uid,run_date, ruleid,query_1, query_2,query_1_result,query_2_result,difference,operator, result,comments)VALUES(%s,'%s','%s','%s','%s','%s','%s','%s','%s','%s',%s,'%s');"""%(config.get('redshift','rs_ops_schema'),config.get('redshift', 'onyx_dq_rule_result_tbl'),batch_id,uid,datetime.datetime.now(),0,'','',0,0,0,'',0,comments)
						insert_query = rs_onyx_cur.execute(result_table_ins_qry)
						update_dq_row_status(config,rs_onyx_conn, rs_onyx_cur, uid, is_data_quality=3)
			#		test_summary = email_dq_results.sql_html(rs_onyx_cur,batch_id,project,project_batch,config)
					uid_results[uid] = rules_result
				logger.info("Final results:"+''+str(uid_results))
							
			else:
				logger.info("Brands not ready in final table")
		else:
			logger.info("Test cases are not present in data quality rules table")
        except Exception as e:
		logger.info(str(traceback.format_exc()))
       		raise
	finally:
		print "inside finally"
		backup = backup_report_tables()
		conn.commit() 
		mysql.close()
		rs_onyx_conn.commit()
        	rs_onyx_cur.close()

def backup_report_tables(bck_dic):
	try:
		backup_success = {}
		backup_failure = {}
        	for key,value in bck_dic.iteritems():
			create_table = eval('report_bck_ddl.%s'%(value))
			create_table=create_table%(report_month)
			tab_creation=rs_onyx_cur.execute(create_table)
			ins_qry = 'insert into test.%s_%s select * from onyx.%s where yyyymm = %s;'%(value,report_month,value,report_month)
			ins_qry = rs_onyx_cur.execute(ins_qry)
			rep_cnt = 'select count(*) from onyx.%s where yyyymm = %s;'%(bck_dic[key],report_month)
			re_cnt_exe=rs_onyx_cur.execute(rep_cnt)
			re_cnt_exe=rs_onyx_cur.fetchall()[0][0]
			bck_cnt = 'select count(*) from test.%s_%s;'%(bck_dic[key],report_month)
			bck_cnt_exe = rs_onyx_cur.execute(bck_cnt)
			bck_cnt_exe=rs_onyx_cur.fetchall()[0][0]
			if re_cnt_exe == bck_cnt_exe:
				backup_table_ins_qry = """INSERT INTO qa.report_bck_lookup(table_name,report_month,run_date,lookup_flag)VALUES('%s','%s','%s','%s');"""%(bck_dic[key],report_month,datetime.datetime.now(),1)
				backup_table_ins_qry=rs_onyx_cur.execute(backup_table_ins_qry)
				backup_success[key] = bck_dic[key]
			else:
				backup_table_ins_qry = """INSERT INTO %s.lookup (report_table_name,report_month,created_on,bck_flag)VALUES('%s','%s','%s','%s');"""%(bck_dic[key],report_month,datetime.datetime.now(),0)
				insert_query = rs_onyx_cur.execute(backup_table_ins_qry)
				backup_failure['key'] = bac_dic[key]
		old_partition = rs_onyx_cur.execute('select count(distinct report_month) from qa.report_bck_lookup;')
		old partition = rs_onyx_cur.fetchall()[0][0]
		if old partition > 4:
			old_report_table = rs_onyx_cur.execute('SELECT report_month from qa.report_bck_lookup order by report_month limit 1')
			old_report_table = old_report_table.rs_onyx_cur.fetchall[0][0]
			if old_report_table:
				erase_backup(bck_dic,old_report_table)
		erase_backup(bck_dic,erase_report_month)
	except:
		print traceback.format_exc()

def erase_backup(bck_dic,erase_report_month):
	try:
		for serial,table in bck_dict.iteritems():
			rs_onyx_cur.execute('drop table qa.%s_%s'%(table,erase_report_month)) 
	except:
		print traceback.format_exc()	



def report_table_setup():
		try:
			bck_dic = {1 : 'monthly_brand_overview'}
			print report_bck_ddl.monthly_brand_overview
			qa_schema = 'qa'
			report_month_exe = rs_onyx_cur.execute('select report_month from onyx.report_info')
			report_month = rs_onyx_cur.fetchall()[0][0]
			bck_check_exe = rs_onyx_cur.execute('select count(*) from qa.report_bck_lookup where report_month in (select report_month from onyx.report_info)')
			bck_check = rs_onyx_cur.fetchall()[0][0]
			if bck_check  == 0:		
				backup_report_tables(bck_dic)	
			elif back_check > 0 and latest_report_table_bck
				rs_onyx_cur.execute('delete from qa.report_bck_lookup where report_month = %s'%(report_month))
				erase_backup(bck_dic,report_month)
				backup_report_tables(bck_dic)
			else:
				print 'Backup table already created for current run %s'%(report_month)
				
		except:
			rs_onyx_conn.commit()
			rs_onyx_cur.close()
			print traceback.format_exc()
if __name__ == '__main__':

	try:

		# Collecting variables

		config = ConfigParser.ConfigParser(allow_no_value=True)
        	basepath = os.path.abspath(os.path.dirname(__file__))
	        head, tail = os.path.split(basepath)
        	config.read(os.path.join(head, 'config', 'wf_aws_onyx_config.ini'))
		function='-main'
		ls_msg=function + '    DESC: '
		sql_host = config.get('mysql', 'wf_host')
		sql_user = config.get('mysql', 'wf_user')
		sql_pwd = config.get('mysql', 'wf_password')
		sql_db= config.get('mysql', 'wf_db')
		rs_host = config.get('redshift', 'rs_host')
		rs_user = config.get('redshift', 'rs_user')
		rs_pwd = config.get('redshift', 'rs_pwd')
		rs_db = config.get('redshift', 'rs_onyx_db')
		rs_port= int(config.get('redshift', 'rs_port'))
		rs_src_schema= config.get('redshift', 'rs_src_schema')

	# Getting log details : 

		log_file_name=config.get("log","data_validation_log_file_name") + "_" +str(datetime.datetime.now().strftime("%Y%m%d-%H")) + ".log"
		error_file_name=config.get("log","data_validation_error_file_name") + "_" +str(datetime.datetime.now().strftime("%Y%m%d-%H")) + ".log"
		application_name=config.get("log","data_validation_application_name")
		module_name=config.get("log","data_validation_module_name")
		logger=utils.log_info(config,True,log_file_name,error_file_name,application_name,module_name)	

	# Getting database connections 

		if len(sql_host.strip()) == 0 or len(sql_user.strip()) == 0 or len(sql_pwd.strip()) == 0 :
			msg='-' + inspect.stack()[0][3] + self.curr_project + ' DESC: '
			sys.exit()
		conn = pymysql.connect(host=sql_host, user=sql_user, passwd=sql_pwd, db=sql_db,cursorclass = pymysql.cursors.DictCursor)
		mysql = conn.cursor(pymysql.cursors.DictCursor)
		rs_onyx_conn = psycopg2.connect(host = rs_host, port = rs_port, dbname = rs_db, user = rs_user, password = rs_pwd)
		rs_onyx_cur = rs_onyx_conn.cursor()
	
	# Collecting arguments 

		project = sys.argv[1]
		project_batch = sys.argv[2]
		batch_id = sys.argv[3]
		basepath = sys.argv[4]
		rid = sys.argv[5]
		cid = sys.argv[6]
		param_file_path = "%s/%s/%s/%s/%s/%s.txt"%(basepath,project,project_batch,batch_id,rid,rid)
		print param_file_path
		bck = backup_report_tables()
#	param_file_path = '/pub/workflow/batch/onyx/spend_analysis_test/1481795357/1/1.txt'
      #  param_file_path = '/pub/workflow/batch/onyx/spend_analysis_v2/1483700776/259/259.txt'
		data = execute(project, project_batch, batch_id, param_file_path)
	except Exception as e:
		raise	
		logger.info(str(traceback.format_exc()))
	
