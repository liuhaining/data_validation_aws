-- Rule 18:-
-- Count of avg cohort active store for current month's week by week

--(note: taking a bigger slice of 5 week to cover whole month. There may be few days extra in beginning and end of month but the same is applied in both the queries. Hence should be same when using exact week by week comparison).

-- query 1 :-Avg of cohort mids reported for the same weeks in onyx_app_tc_customer_status_weekly should be 

-- same 

-- query 2 :-when compared to view nya_joe_restaurant.weekly_brand_performance_trend



select uid, AVG(cohort_mids) AS AVR_COHORT_MIDS from PARAM_RS_ONYX_SCHEMA:.onyx_app_tc_customer_status_weekly where uid = %s  and year=PARAM_CURRENT_YEAR: and week IN ('35', '36', '37', '38', '39') group by uid;
      
