-- Rule 20:-

--  Count of customers in onyx_app_tc_customer_status_monthly
--  is always more than 
--  Count of customer of type home in monthly_store_geo_stats table (because this table only stores top 5 zip counts).

-- query 1 :- q1 = select uid, mid, SUM(cards) from onyx_app_tc_customer_status_monthly where uid='nya_joe_restaurant' and year='2016' and month='9' group by uid, mid  order by mid;

-- query 2 :- q2 = select uid, mid, sum(value) from nya_joe_restaurant.monthly_store_geo_stats where type_of_result = 'home' and year = '2016' and month = '9' group by uid, mid order by mid;

-- q1 > q2


select uid, mid, SUM(cards) from PARAM_RS_ONYX_SCHEMA:.onyx_app_tc_customer_status_monthly where uid=%s  and year=PARAM_CURRENT_YEAR: and month=PARAM_CURRENT_MONTH: group by uid, mid  order by mid;
      


